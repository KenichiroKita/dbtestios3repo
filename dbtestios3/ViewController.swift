//
//  ViewController.swift
//  dbtestios3
//
//  Created by 北健一郎 on 2015/11/18.
//  Copyright (c) 2015年 kenichirokita. All rights reserved.
//

import UIKit

struct Items {
    var name:String
    
}


class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBAction func unWindToTop(segue: UIStoryboardSegue) {}

    @IBOutlet weak var tableview: UITableView!
    
    
    var items:[Items] = []
    
    //required initしないとエラーになる。
    required init(coder aDecoder: NSCoder) {
        self.items = []
        super.init(coder: aDecoder)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableview.delegate = self
        tableview.dataSource = self
        // Do any additional setup after loading the view, typically from a nib.
        
        
        
        
        
        
        //jsonデータ取得
        requestApi()
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    
    func requestApi() {
        
        let manager = AFHTTPSessionManager()
        
        
        
        
        let url = "http://160.16.237.233/test1/dbtest3.php"
        
        
        manager.GET(url, parameters: nil,
            success: { (operation, json) -> Void in
                let dict = json as! NSArray
                
                for arr in dict {
                    //structで整形
                    var arrData = Items(name: arr["name"] as! String)
                    
                    //配列に追加
                    self.items.append(arrData)
                }
                
                
                
                //tableViewをリロード
                self.tableview.reloadData()
                
            }, failure: { (operation, error) -> Void in
                // エラー
                println(error)
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // セルの行数
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    
    //セルの内容を変更
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "Cell")
        
        cell.textLabel?.text = (items[indexPath.row] as Items).name
        return cell
    }
    


}

